# Collaboard Client

## Installation

```bash
	git clone https://asadnadeem@bitbucket.org/asadnadeem/collaboard-client.git
	cd collaboard-client
	
	npm install -g concurrently webpack webpack-dev-server
	npm install
	npm run watch
	
	# production build and run
	NODE_ENV=production npm run build
	NODE_ENV=production npm run browser  
```