import ChatActions from "./ChatActions";
import SessionActions from "./SessionActions";
import BoardActions from "./BoardActions";
import ToolboxActions from "./ToolboxActions";
import InviteActions from "./InviteActions";

export default {
  board: BoardActions,
  chat: ChatActions,
  session: SessionActions,
  toolbox: ToolboxActions,
  invite: InviteActions
};