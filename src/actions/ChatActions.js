import {SEND_MESSAGE, RECEIVE_MESSAGE, RECEIVE_ALL_MESSAGES} from "constants/ChatConstants";

import socket from "services/socket";

export default {
  sendMessage: (message) => {
    socket.sendMessage(message);

    window.flux.dispatcher.dispatch({
      type: SEND_MESSAGE,
      payload: message
    });

  },

  receiveMessage: (message) => {
    window.flux.dispatcher.dispatch({
      type: RECEIVE_MESSAGE,
      payload: message
    });
  },

  receiveAllMessages: (messages) => {
    window.flux.dispatcher.dispatch({
      type: RECEIVE_ALL_MESSAGES,
      payload: messages
    });
  }
};