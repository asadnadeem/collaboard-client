import {TOOL_SELECT, FILL_COLOR_CHANGE, STROKE_COLOR_CHANGE, STROKE_WIDTH_CHANGE} from "constants/ToolboxConstants";

import socket from "services/socket";

export default {
  toolSelect: (tool) => {
    window.flux.dispatcher.dispatch({
      type: TOOL_SELECT,
      payload: tool
    });
  },

  fillColorChange: (color) => {
    window.flux.dispatcher.dispatch({
      type: FILL_COLOR_CHANGE,
      payload: color
    });
  },

  strokeColorChange: (color) => {
    window.flux.dispatcher.dispatch({
      type: STROKE_COLOR_CHANGE,
      payload: color
    });
  },

  strokeWidthChange: (width) => {
    window.flux.dispatcher.dispatch({
      type: STROKE_WIDTH_CHANGE,
      payload: width
    });
  }
};