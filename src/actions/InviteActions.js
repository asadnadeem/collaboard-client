import {INVITE} from "constants/InviteConstants";

import socket from "services/socket";

export default {
  invite: (recipient) => {
    socket.invite(recipient);
  }
};