import {SEND_OBJECT, RECEIVE_OBJECT, RECEIVE_ALL_OBJECTS,
        UNDO_SELF_OBJECT, OBJECT_ACK, DELETE_OBJECT,
        DELETE_FOREIGN_OBJECT, MOVE_OBJECT, OBJECT_UPDATE} from "constants/BoardConstants";

import socket from "services/socket";

export default {
  sendObject: (object) => {
    object._creatorLocalId = window.flux.stores.BoardStore.getNewCreatorLocalId();
    socket.sendObject(object);

    window.flux.dispatcher.dispatch({
      type: SEND_OBJECT,
      payload: object
    });

  },

  receiveObject: (object) => {
    window.flux.dispatcher.dispatch({
      type: RECEIVE_OBJECT,
      payload: object
    });
  },

  objectAck: (object) => {
    window.flux.dispatcher.dispatch({
      type: OBJECT_ACK,
      payload: object
    });
  },

  objectUpdate: (object, fromServer) => {
    if (!fromServer) socket.updateObject(object);

    window.flux.dispatcher.dispatch({
      type: OBJECT_UPDATE,
      payload: {object, fromServer}
    });
  },

  undoSelfObject: () => {
    let selfObjects = window.flux.stores.BoardStore.getSelfObjects();
    let selfObjectsKeys = Object.keys(selfObjects);
    if (!selfObjectsKeys.length) return;

    let objectId = selfObjects[selfObjectsKeys[selfObjectsKeys.length - 1]]._id;

    window.flux.actions.board.deleteObject(objectId);
  },

  deleteObject: (objectId) => {
    socket.deleteObject(objectId);

    window.flux.dispatcher.dispatch({
      type: DELETE_OBJECT,
      payload: objectId
    });
  },

  deleteForeignObject: (objectId) => {
    window.flux.dispatcher.dispatch({
      type: DELETE_FOREIGN_OBJECT,
      payload: objectId
    });
  },

  receiveAllObjects: (objects) => {
    window.flux.dispatcher.dispatch({
      type: RECEIVE_ALL_OBJECTS,
      payload: objects
    });
  }
};