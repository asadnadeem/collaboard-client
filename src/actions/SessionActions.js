import {CHANGE_NAME, CHANGE_ROOM} from "constants/SessionConstants";

import socket from "services/socket";

export default {
  changeName: (name) => {
    socket.changeName(name);

    window.flux.dispatcher.dispatch({
      type: CHANGE_NAME,
      payload: name
    });

  },

  changeRoom: (room) => {
    socket.changeRoom(room);

    window.flux.dispatcher.dispatch({
      type: CHANGE_ROOM,
      payload: room
    });

  },

  nameChanged: (name) => {
    window.localStorage.setItem('name', name);

    window.flux.dispatcher.dispatch({
      type: CHANGE_NAME,
      payload: name
    });

  },

  roomChanged: (room) => {
    window.localStorage.setItem('room', room);

    window.flux.dispatcher.dispatch({
      type: CHANGE_ROOM,
      payload: room
    });
  },
};