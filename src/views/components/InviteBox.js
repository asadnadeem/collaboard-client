import React from "react";
import classnames from "classnames";


class InviteBox extends React.Component {

  constructor() {
    super();

    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.handleInviteButtonClick = this.handleInviteButtonClick.bind(this);

    this.state = {};
  }

  handleFormSubmit(ev) {
    ev.preventDefault();

    this.setState({
      sent: true
    });

    window.flux.actions.invite.invite(React.findDOMNode(this.refs.emailInput).value);
  }

  handleInviteButtonClick(ev) {
    this.setState({
      promptOpen: !this.state.promptOpen
    })
  }
  
  render () {
    let classes = classnames({
      "invite-box--sent": this.state.sent,
      "invite-box--prompt-open": this.state.promptOpen,
      "invite-box": true
    });

    return (
      <div className={classes}>
        <input type="button" value="Invite..." onClick={this.handleInviteButtonClick} />

        <div className="invite-box__prompt">
          {this.state.sent && (
            <p>Sent!</p>
          )}
          <form onSubmit={this.handleFormSubmit}>
            <input ref="emailInput" type="email" />
            <input type="submit" value="Send Invitation" />
          </form>
        </div>
      </div>
    );
  }

}


export default InviteBox;