import React from "react";
import classnames from "classnames";
import Fluxxor from "fluxxor";
import ColorPicker from "./ColorPicker";

let FluxMixin = Fluxxor.FluxMixin;
let StoreWatchMixin = Fluxxor.StoreWatchMixin;


class Toolbox extends React.Component {
  constructor() {
    super();

    this.handleToolboxClick = this.handleToolboxClick.bind(this);
    this.handleUndoClick = this.handleUndoClick.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.handleFillColorChange = this.handleFillColorChange.bind(this);
    this.handleStrokeWidthChange = this.handleStrokeWidthChange.bind(this);
  }

  handleKeyDown (ev) {
    switch(ev.keyCode) {
      case 49:
        window.flux.actions.toolbox.toolSelect('stroke');
        break;

      case 50:
        window.flux.actions.toolbox.toolSelect('line');
        break;

      case 51:
        window.flux.actions.toolbox.toolSelect('box');
        break;

      case 52:
        window.flux.actions.toolbox.toolSelect('circle');
        break;
    }
  }

  componentDidMount () {
    window.addEventListener("keydown", this.handleKeyDown);
  }

  componentWillUnmount () {
    window.removeEventListener("keydown", this.handleKeyDown);
  }

  handleToolboxClick(ev) {
    window.flux.actions.toolbox.toolSelect(ev.currentTarget.getAttribute('data-value'));
  }

  handleUndoClick(ev) {
    window.flux.actions.board.undoSelfObject();
  }

  handleFillColorChange(color) {
    window.flux.actions.toolbox.fillColorChange(color);
  }

  handleStrokeColorChange(color) {
    window.flux.actions.toolbox.strokeColorChange(color);
  }

  handleStrokeWidthChange(ev) {
    window.flux.actions.toolbox.strokeWidthChange(ev.currentTarget.value);
  }

  getToolClasses(tool) {
    let toolClass = "";

    switch(tool) {
      case 'stroke': toolClass = "pencil"; break;
      case 'line': toolClass = "minus"; break;
      case 'box': toolClass = "square-o"; break;
      case 'circle': toolClass = "circle-thin"; break;
      case 'text': toolClass = "font"; break;
      case 'image': toolClass = "picture-o"; break;
      case 'eraser': toolClass = "eraser"; break;
      case 'undo': toolClass = "undo"; break;
      case 'selection': toolClass = "hand-o-up"; break;
    }

    return classnames({
      ["fa-" + toolClass]: "true",
      "fa": true
    });
  }

  render () {
    let undoClasses = classnames({
      'toolbox__tool': true,
      'toolbox__tool--undo': true,
      'toolbox__tool--disabled': this.props.canUndo
    });

    let i = 0;

    return (
      <div className="toolbox">
        <ul className="toolbox__section toolbox__tools">
          {this.props.tools.map((tool) => {
            let classes = classnames({
              "toolbox__tool": true,
              ["toolbox__tool--" + tool]: true,
              "toolbox__tool--selected": tool === this.props.currTool
            });

            return (
              <li className={classes} key={i++} onClick={this.handleToolboxClick} data-value={tool}>
                <span className="toolbox__tool-icon">
                  <i className={this.getToolClasses(tool)}></i>
                </span>
              </li>
            );
          })}
        </ul>

        <div className="toolbox__section toolbox__stroke">
          <input type="number" className="toolbox__numeric-input" value={this.props.currStrokeWidth} min="1" max="99" onChange={this.handleStrokeWidthChange}></input>
        </div>

        <div className="toolbox__section toolbox__colors">
          <ColorPicker color={this.props.currStrokeColor} onChange={this.handleStrokeColorChange} />
          <ColorPicker color={this.props.currFillColor} onChange={this.handleFillColorChange} />
        </div>

        <div className="toolbox__section toolbox__undoredo">
          <div className={undoClasses} onClick={this.handleUndoClick} data-value={"undo"}>
            <span className="toolbox__tool-icon">
              <i className={this.getToolClasses("undo")}></i>
            </span>
          </div>
        </div>
      </div>
    );
  }

}


export default Toolbox;