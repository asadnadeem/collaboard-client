import React from "react";
import classnames from "classnames";
import Fluxxor from "fluxxor";
import ReactColorPicker from "react-color-picker";

let FluxMixin = Fluxxor.FluxMixin;
let StoreWatchMixin = Fluxxor.StoreWatchMixin;


class ColorPicker extends React.Component {
  constructor() {
    super();

    this.handleSwatchClick = this.handleSwatchClick.bind(this);
    this.handleColorPickerDrag = this.handleColorPickerDrag.bind(this);
    this.handleWindowClick = this.handleWindowClick.bind(this);

    this.state = {
      selectorOpen: false
    };
  }

  componentDidMount () {
    window.addEventListener("click", this.handleWindowClick);
  }

  componentWillUnmount () {
    window.removeEventListener("click", this.handleWindowClick);
  }

  handleWindowClick(ev) {
    if (this.state.selectorOpen) {
      this.setState({
        selectorOpen: false
      });
    }
  }

  handleSwatchClick(ev) {
    ev.stopPropagation();

    this.setState({
      selectorOpen: !this.state.selectorOpen
    });
  }

  handleColorPickerDrag(color) {
    this.props.onChange(color);
  }

  render () {
    let swatchStyles = {
      backgroundColor: this.props.color
    };

    let selectClasses = classnames({
      'colorpicker__selector': true,
      'colorpicker__selector--open': !!this.state.selectorOpen
    });

    return (
      <div className="colorpicker">
        <div className="colorpicker__swatch" style={swatchStyles} onClick={this.handleSwatchClick}></div>
        <div className={selectClasses}>
          <ReactColorPicker
            saturationWidth={200} 
            saturationHeight={200} 
            hueHeight={200}
            onDrag={this.handleColorPickerDrag} />
        </div>
      </div>
    );
  }

}


export default ColorPicker;