import React from "react";
import InviteBox from "views/components/InviteBox";

class AppHeader extends React.Component {

  constructor() {
    super();

    this.handleRoomDisplayClick = this.handleRoomDisplayClick.bind(this);
    this.handleNameDisplayClick = this.handleNameDisplayClick.bind(this);

    this.handleRoomFieldBlur = this.handleRoomFieldBlur.bind(this);
    this.handleNameFieldBlur = this.handleNameFieldBlur.bind(this);

    this.handleFieldKeyDown = this.handleFieldKeyDown.bind(this);

    this.handleRoomFieldChange = this.handleRoomFieldChange.bind(this);
    this.handleNameFieldChange = this.handleNameFieldChange.bind(this);

    this.handleSaveAsImageClick = this.handleSaveAsImageClick.bind(this);

    this.state = {
      editingName: false,
      editingRoom: false
    }
  }

  componentWillReceiveProps (props) {
    this.setState({
      name: props.name,
      room: props.room
    });
  }

  handleRoomDisplayClick () {
    this.setState({
      editingRoom: true,
      startRoom: this.state.room
    });
  }

  handleNameDisplayClick () {
    this.setState({
      editingName: true,
      startName: this.state.name
    });
  }

  handleRoomFieldBlur () {
    if (this.state.room != this.state.startRoom) {
      var flux = window.flux;
      flux.actions.session.changeRoom(this.state.room);
    }

    this.setState({
      editingRoom: false,
      startRoom: null
    });
  }

  handleNameFieldBlur () {
    if (this.state.name != this.state.startName) {
      var flux = window.flux;
      flux.actions.session.changeName(this.state.name);
    }

    this.setState({
      editingName: false,
      startName: null
    });
  }

  handleRoomFieldChange (ev) {
    ev.stopPropagation();

    this.setState({
      room: ev.currentTarget.value
    });
  }

  handleNameFieldChange (ev) {
    ev.stopPropagation();

    this.setState({
      name: ev.currentTarget.value
    }); 
  }

  handleFieldKeyDown (ev) {
    if (ev.keyCode === 13) {
      ev.currentTarget.blur();
    }
  }

  handleSaveAsImageClick (ev) {
    let canvas = document.getElementsByTagName("canvas")[0];

    let dataURL = canvas.toDataURL('image/png');
    ev.currentTarget.href = dataURL;
  }

  render () {
    return (
      <div className="appheader">
        <div className="appheader__column">
          <img className="appheader__logo" src="images/logo.png"></img>
        </div>

        <div className="appheader__column">
          <span className="appheader__column-icon"><i className="fa fa-users"></i></span>
          {!this.state.editingRoom ? (
            <span ref="roomDisplay" className="appheader__column-content" onClick={this.handleRoomDisplayClick}>{this.state.room}</span>
          ) : (
            <input type="name" value={this.state.room} className="appheader__column-content appheader__column-input" onBlur={this.handleRoomFieldBlur} onChange={this.handleRoomFieldChange} onKeyDown={this.handleFieldKeyDown} autoFocus></input>
          )}
        </div>

        <div className="appheader__column">
          <InviteBox />
        </div>

        <div className="appheader__column">
          <span className="appheader__column-icon"><i className="fa fa-user"></i></span>
          {!this.state.editingName ? (
            <span ref="nameDisplay" className="appheader__column-content" onClick={this.handleNameDisplayClick}>{this.state.name}</span>
          ) : (
            <input type="text" value={this.state.name} className="appheader__column-content appheader__column-input" onBlur={this.handleNameFieldBlur} onChange={this.handleNameFieldChange} onKeyDown={this.handleFieldKeyDown} autoFocus></input>
          )}
        </div>

        <div className="appheader__column">
          <a href="#" className="btn" download="collaboard-canvas.png" onClick={this.handleSaveAsImageClick}>Save as Image...</a>
        </div>
      </div>
    );
  }

}


export default AppHeader;