import React from "react";
import classnames from "classnames";


class ChatBox extends React.Component {

  constructor() {
    super();

    this.handleSendMessageClick = this.handleSendMessageClick.bind(this);
    this.handleChatboxToggleBtn = this.handleChatboxToggleBtn.bind(this);

    this.state = {
      chatboxOpen: false
    };
  }

  handleSendMessageClick(ev) {
    ev.preventDefault();
    ev.stopPropagation();

    let flux = window.flux;

    var messageField = React.findDOMNode(this.refs.messageField);
    if (!messageField.value.length) return;

    flux.actions.chat.sendMessage(messageField.value);
    messageField.value = "";
  }

  handleChatboxToggleBtn(ev) {
    ev.stopPropagation();

    this.setState({
      chatboxOpen: !this.state.chatboxOpen
    });

    React.findDOMNode(this.refs.messageField).focus();
  }
  
  render () {
    let chatboxClasses = classnames({
      'chatbox': true,
      'chatbox--open': this.state.chatboxOpen
    });

    let chatboxIconClasses = classnames({
      'fa': true,
      'fa-chevron-up': !this.state.chatboxOpen,
      'fa-chevron-down': this.state.chatboxOpen
    })

    return (
      <div className={chatboxClasses}>
        <div className="chatbox__header" onClick={this.handleChatboxToggleBtn}>
          <span>Chat</span>

          <span className="chatbox__toggle_btn">
            <i className={chatboxIconClasses}></i>
          </span>
        </div>

        <div className="chatbox__body">
          <div className="chatbox__messages-wrap">
            <ul className="chatbox__messages">
              {(this.props.messages && this.props.messages.map((message) => {
                let messageClasses = classnames({
                  'chatbox__message': true,
                  'chatbox__message--own': !!message.own
                });

                return (
                  <li className={messageClasses}>
                    <span className="chatbox__message-text">{message.text}</span>
                    <span className="chatbox__message-author">{message.username}</span>
                  </li>
                );
              }))}
            </ul>
          </div>

          <form className="chatbox__inputform">
            <input ref="messageField" type="text" className="chatbox__messagefield"></input>
            <input type="submit" onClick={this.handleSendMessageClick} value="Send" className="chatbox__sendbtn"></input>
          </form>
        </div>
      </div>
    );
  }

}


export default ChatBox;