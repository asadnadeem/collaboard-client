import React from "react";
import {Group, Shape, Surface, Text, Transform, Path} from "react-art";
import Circle from "react-art/lib/Circle.art";
import classnames from "classnames";
import _ from "lodash";

class Board extends React.Component {

  constructor() {
    super();

    this.handleCanvasMouseDown = this.handleCanvasMouseDown.bind(this);
    this.handleCanvasMouseMove = this.handleCanvasMouseMove.bind(this);
    this.handleCanvasMouseUp = this.handleCanvasMouseUp.bind(this);
    this.handleNewTextElementKeyDown = this.handleNewTextElementKeyDown.bind(this);
    this.handleNewTextBlur = this.handleNewTextBlur.bind(this);

    this.handleCanvasTouchStart = this.handleCanvasTouchStart.bind(this);
    this.handleCanvasTouchMove = this.handleCanvasTouchMove.bind(this);
    this.handleCanvasTouchEnd = this.handleCanvasTouchEnd.bind(this);

    this.state = {
      selection: null
    };
  }

  handleCanvasMouseDown (ev) {
    var mouseX = ev.clientX - React.findDOMNode(this.refs.container).offsetLeft + window.scrollX;
    var mouseY = ev.clientY - React.findDOMNode(this.refs.container).offsetTop + window.scrollY;

    let newObject, selection, newState = {};

    if (this.props.currTool == "selection") {
      selection = this.getCollidingObject(mouseX, mouseY);
      newState.selection = selection;
    } else if (!this.state.newObject) {
      newObject = this.createNewObject(mouseX, mouseY);
      newState.newObject = newObject;
    }

    Object.assign(newState, {
      mouseDown: true,
      lastMouseX: mouseX,
      lastMouseY: mouseY
    });

    this.setState(newState);
  }

  handleCanvasMouseMove (ev) {
    if (!this.state.mouseDown) return;

    var mouseX = ev.clientX - React.findDOMNode(this.refs.container).offsetLeft + window.scrollX;
    var mouseY = ev.clientY - React.findDOMNode(this.refs.container).offsetTop + window.scrollY;

    if (Math.sqrt( Math.pow( Math.abs(mouseX - this.state.lastMouseX), 2) + Math.pow( Math.abs(mouseY - this.state.lastMouseY), 2) ) <= 2) return;

    let newObject;

    if (this.state.selection) {
      this.moveObject(this.state.selection, mouseX - this.state.lastMouseX, mouseY - this.state.lastMouseY);
    } else if (this.state.newObject) {
      if (this.state.newObject.type === "text") return;

      newObject = this.updateNewObject(mouseX, mouseY, this.state.newObject);
    }
    
    this.setState({
      newObject: newObject,
      lastMouseX: mouseX,
      lastMouseY: mouseY
    });
  }

  handleCanvasMouseUp (ev) {
    if (!this.state.mouseDown) return;

    var mouseX = ev.clientX - React.findDOMNode(this.refs.container).offsetLeft + window.scrollX;
    var mouseY = ev.clientY - React.findDOMNode(this.refs.container).offsetTop + window.scrollY;

    let newObject
      , newState = {};

    if (this.state.selection) {
    } else if (this.state.newObject && this.state.newObject.type !== "text") {
      newObject = this.finalizeObject(mouseX, mouseY, this.state.newObject);

      let flux = window.flux;
      flux.actions.board.sendObject(newObject);

      newState.newObject = null;
    }

    newState.selection = null;
    newState.mouseDown = false;
    
    this.setState(newState);
  }

  handleCanvasTouchStart (ev) {
    this.handleCanvasMouseDown({ clientX: ev.changedTouches[0].clientX, clientY: ev.changedTouches[0].clientY });
  }

  handleCanvasTouchMove (ev) {
    this.handleCanvasMouseMove({ clientX: ev.changedTouches[0].clientX, clientY: ev.changedTouches[0].clientY });
  }

  handleCanvasTouchEnd (ev) {
    this.handleCanvasMouseUp({ clientX: ev.changedTouches[0].clientX, clientY: ev.changedTouches[0].clientY });
  }

  handleNewTextElementKeyDown(ev) {
    if (ev.keyCode !== 13) return;

    let newObject = this.state.newObject;
    this.state.newObject.text = ev.currentTarget.value;

    let flux = window.flux;
    flux.actions.board.sendObject(newObject);

    this.setState({
      newObject: null,
      selection: null,
      mouseDown: false
    })
  }

  handleNewTextBlur(ev) {
    let newObject = this.state.newObject;
    this.state.newObject.text = ev.currentTarget.value;

    let flux = window.flux;
    flux.actions.board.sendObject(newObject);

    this.setState({
      newObject: null,
      selection: null,
      mouseDown: false
    })
  }

  getCollidingObject(x, y) {
    let collidingObject;

    this.props.objects.some((object) => {
      let bbox = this.getObjectBoundingBox(object);

      if (x > bbox.x1 && x < bbox.x2 && y > bbox.y1 && y < bbox.y2) {
        return (collidingObject = object);
      }
    });

    return collidingObject;
  }

  getObjectBoundingBox(object) {
    switch(object.type) {
      case 'stroke':
        var mx=0, my=0, mmx=0, mmy=0;
        object.points.map((p) => {
          if (p.x < mx) mx = p.x;
          if (p.y < my) my = p.y;
          if (p.x > mmx) mmx = p.x;
          if (p.y > mmy) mmy = p.y;
        });

        return {x1: mx, y1: mx, x2: mmx, y2: mmy};

      case 'box':
        return {x1: object.x, y1: object.y, x2: object.x + object.width, y2: object.y + object.height};

      case 'circle':
        return {x1: object.x - object.radius / 2, y1: object.y - object.radius / 2, x2: object.x + object.radius / 2, y2: object.y + object.radius / 2};

      case 'line':
        return {x1: object.x1, y1: object.y1, x2: object.x2, y2: object.y2};
    } 
  }

  componentDidUpdate() {
    if (this.refs.newTextElement) {
      let self = this;
      setTimeout(function() {
        React.findDOMNode(self.refs.newTextElement).focus();
      }, 50);
    }
  }

  render () {
    if (__SERVER__) {
      return (
        <div id="canvas-container" className="board" ref='container' style={{ height: 700, position: "relative" }} onMouseDown={this.handleCanvasMouseDown} onMouseUp={this.handleCanvasMouseUp} onMouseMove={this.handleCanvasMouseMove}>
          <Surface width="1500" height="700">
          </Surface>
        </div>
      );
    }

    return (
      <div id="canvas-container" className="board" ref='container' style={{ height: 700, position: "relative" }} onMouseDown={this.handleCanvasMouseDown} onMouseUp={this.handleCanvasMouseUp} onMouseMove={this.handleCanvasMouseMove} onTouchStart={this.handleCanvasTouchStart} onTouchMove={this.handleCanvasTouchMove} onTouchEnd={this.handleCanvasTouchEnd}>
        {this.state.newObject && this.state.newObject.type === "text" && (
          <input ref="newTextElement" type="text" onKeyDown={this.handleNewTextElementKeyDown} onBlur={this.handleNewTextBlur} style={{
            position: "absolute",
            left: this.state.newObject.x - 7, // subtract the padding
            top: this.state.newObject.y - 8, // subtract the padding
            font: "inherit",
            fontFamily: "Arial",
            fontSize: this.state.newObject.fontSize + 'px',
            color: this.state.newObject.fillColor,
            padding: "5px"
          }} />
        )}

        <Surface width="1500" height="700" fill="#FFF">
          {this.props.objects.map((object) => {
            let selected = this.state.selection ? object._id === this.state.selection._id : null;

            switch(object.type) {
              case 'stroke': case 'eraser': return this.getStrokeElement(object, selected); break;
              case 'line': return this.getLineElement(object, selected); break;
              case 'box': return this.getBoxElement(object, selected); break;
              case 'text': return this.getTextElement(object, selected); break;
              case 'circle': return this.getCircleElement(object, selected); break;
            }
          })}

          {
            this.state.newObject && (() => {
              let object = this.state.newObject;
              switch(object.type) {
                case 'stroke': case 'eraser': return this.getStrokeElement(object, true); break;
                case 'line': return this.getLineElement(object, true); break;
                case 'box': return this.getBoxElement(object, true); break;
                case 'circle': return this.getCircleElement(object, true); break;
              }
            }())
          }
        </Surface>
      </div>
    );
  }

  createStrokePoint(command, point) {
    if (typeof command !== "string") {
      point = command;
      command = "";
    }

    return command + point.x + "," + point.y;
  }

  getStrokeElement (data) {
    if (!data || !data.points || !data.points.length || data.points.length < 2) return;

    var d = [];
    d.push( this.createStrokePoint( "M", data.points[0] ) );
    d.push( this.createStrokePoint( "L", data.points[1] ) );

    for (var i = 2; i < data.points.length; i++) {
      d.push( this.createStrokePoint( data.points[i] ) );
    }

    d = d.join(' ');
    
    return (
      <Shape stroke={data.strokeColor} strokeWidth={data.strokeWidth} d={d} />
    );
  }

  getLineElement (data) {
    if (!data || !data.x1 || !data.y2 || !data.x2 || !data.y2) return;
    
    var d = [];
    d.push( this.createStrokePoint( "M", { x: data.x1, y: data.y1 } ) );
    d.push( this.createStrokePoint( "L", { x: data.x2, y: data.y2 } ) );
    d = d.join(' ');

    return (
      <Shape stroke={data.strokeColor} strokeWidth={data.strokeWidth} d={d} />
    );
  }

  getBoxElement (data, selected) {
    if (!data || !data.x || !data.y || !data.width || !data.height) return;
    
    var d = [];
    d.push( this.createStrokePoint( "M", { x: data.x, y: data.y } ) );
    d.push( this.createStrokePoint( "L", { x: data.x + data.width, y: data.y } ) );
    d.push( this.createStrokePoint( { x: data.x + data.width, y: data.y + data.height } ) );
    d.push( this.createStrokePoint( { x: data.x, y: data.y + data.height } ) );
    d.push( this.createStrokePoint( { x: data.x, y: data.y } ) );
    d = d.join(' ');

    return (
      <Group>
        <Shape stroke={data.strokeColor} strokeWidth={data.strokeWidth} fill={data.fillColor} d={d} />
      </Group>
    );
  }

  getCircleElement (data) {
    if (!data || !data.x || !data.y || !data.radius) return;
    
    var path = Path().moveTo(data.x, data.y - data.radius)
        .arc(0, data.radius * 2, data.radius)
        .arc(0, data.radius * -2, data.radius)
        .close();

    return (
      <Shape stroke={data.strokeColor} strokeWidth={data.strokeWidth} fill={data.fillColor} d={path} />
    );
  }

  getTextElement (data) {
    if (!data || !data.x || !data.y || !data.text) return;
    
    var path = Path().moveTo(data.x, data.y).lineTo(data.x + 100, data.y).close();

    return (
      <Text fill={data.fillColor} font={'normal ' + data.fontSize + 'px "Arial"'} x={data.x} y={data.y}>{data.text}</Text>
    );
  }

  //////

  createNewObject (mouseX, mouseY) {
    switch(this.props.currTool) {
      case 'stroke' :
        return {
          type: 'stroke',
          strokeColor: this.props.currStrokeColor,
          strokeWidth: this.props.currStrokeWidth,
          fillColor: this.props.currFillColor,
          points: [
            { x: mouseX, y: mouseY }
          ]
        };

        break;

      case 'eraser' :
        return {
          type: 'eraser',
          strokeColor: '#FFFFFF',
          strokeWidth: this.props.currStrokeWidth,
          points: [
            { x: mouseX, y: mouseY }
          ]
        };

        break;

      case 'line' :
        return {
          type: 'line',
          strokeColor: this.props.currStrokeColor,
          strokeWidth: this.props.currStrokeWidth,
          x1: mouseX,
          y1: mouseY
        };

      case 'box' :
        return {
          type: 'box',
          strokeColor: this.props.currStrokeColor,
          strokeWidth: this.props.currStrokeWidth,
          fillColor: this.props.currFillColor,
          x: mouseX,
          y: mouseY
        };

        break;

      case 'circle' :
        return {
          type: 'circle',
          strokeColor: this.props.currStrokeColor,
          strokeWidth: this.props.currStrokeWidth,
          fillColor: this.props.currFillColor,
          x: mouseX,
          y: mouseY
        };

        break;

      case 'text' :
        return {
          type: 'text',
          fillColor: this.props.currFillColor,
          fontSize: 11 + Number(this.props.currStrokeWidth),
          x: mouseX,
          y: mouseY
        };

        break;
    }
  }

  updateNewObject (mouseX, mouseY, existingObject) {
    switch(existingObject.type) {
      case 'stroke':  case 'eraser':
        existingObject.points.push({ x: mouseX, y: mouseY });

        break;

      case 'line': 
        existingObject.x2 = mouseX;
        existingObject.y2 = mouseY;

        break;

      case 'box': 
        existingObject.width = mouseX - existingObject.x;
        existingObject.height = mouseY - existingObject.y;

        break;

      case 'circle': 
        existingObject.radius = Math.sqrt( Math.pow(existingObject.y - mouseY, 2) + Math.pow(existingObject.x - mouseX, 2) );

        break;
    }

    return existingObject;
  }

  finalizeObject (mouseX, mouseY, existingObject) {
    return existingObject;
  }

  moveObject (object, dx, dy) {
    object.x += dx;
    object.y += dy;

    let flux = window.flux;
    flux.actions.board.objectUpdate(object);
  }
}


export default Board;