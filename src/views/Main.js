import React from "react";
import Transmit from "react-transmit";
import ChatBox from "views/components/ChatBox";
import Board from "views/components/Board";
import Toolbox from "views/components/Toolbox";
import AppHeader from "views/components/AppHeader";
import socket from "services/socket";
import Fluxxor from "fluxxor";

let FluxMixin = Fluxxor.FluxMixin;
let StoreWatchMixin = Fluxxor.StoreWatchMixin;


let Main = React.createClass({

	mixins: [ FluxMixin(React), StoreWatchMixin("BoardStore", "ChatStore", "SessionStore", "ToolboxStore") ],

	getStateFromFlux () {
		let flux = this.getFlux();
		
		return {
			boardObjects: flux.store("BoardStore").getObjects(),
			chatMessages: flux.store("ChatStore").getMessages(),
			name: flux.store("SessionStore").getName(),
			room: flux.store("SessionStore").getRoom(),

			tools: flux.store("ToolboxStore").getTools(),
			currTool: flux.store("ToolboxStore").getCurrTool(),

			currFillColor: flux.store("ToolboxStore").getCurrFillColor(),
			currStrokeColor: flux.store("ToolboxStore").getCurrStrokeColor(),
			currStrokeWidth: flux.store("ToolboxStore").getCurrStrokeWidth(),

			canUndo: flux.store("BoardStore").canUndo()
		};
	},

	contextTypes: {
    router: React.PropTypes.func
  },

	componentDidMount () {
		if (__SERVER__) return;

		socket.connect();

		let flux = this.getFlux();

		if (window.localStorage.getItem('name')) {
			flux.actions.session.changeName(window.localStorage.getItem('name'));
		}

		if (this.context.router.getCurrentQuery().room) {
			flux.actions.session.changeRoom(this.context.router.getCurrentQuery().room);
		} else if (window.localStorage.getItem('room')) {
			flux.actions.session.changeRoom(window.localStorage.getItem('room'));
		}
	},

	render () {
		let toolboxAttrs = {
			tools: this.state.tools,
			currTool: this.state.currTool,

			currFillColor: this.state.currFillColor,
			currStrokeColor: this.state.currStrokeColor,
			currStrokeWidth: this.state.currStrokeWidth,

			canUndo: this.state.canUndo
		};

		return (
			<div>
				<AppHeader name={this.state.name} room={this.state.room} />
				<Toolbox {... toolboxAttrs} />
				<Board objects={this.state.boardObjects} {... toolboxAttrs} />
				<ChatBox messages={this.state.chatMessages} />
			</div>
		);
	}


});


export default Transmit.createContainer(Main);