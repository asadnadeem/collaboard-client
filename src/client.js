import React from "react";
import Router from "react-router";
import routes from "views/Routes";
import Transmit from "react-transmit";
import Fluxxor from "fluxxor";
import stores from "./stores/stores";
import actions from "./actions/actions";

/**
 * Init fluxxor
 */
let flux = new Fluxxor.Flux(stores, actions);
window.flux = flux;

flux.on("dispatch", function(type, payload) {
  console.log("[Dispatch]", type, payload);
});


/**
 * Fire-up React Router.
 */
Router.run(routes, Router.HistoryLocation, (Handler) => {
	Transmit.render(Handler, {flux: flux}, document.getElementById("react-root"));
});

/**
 * Detect whether the server-side render has been discarded due to an invalid checksum.
 */
if (process.env.NODE_ENV !== "production") {
	const reactRoot = window.document.getElementById("react-root");

	if (!reactRoot || !reactRoot.firstChild || !reactRoot.firstChild.attributes ||
	    !reactRoot.firstChild.attributes["data-react-checksum"]) {
		console.error("Server-side React render was discarded. Make sure that your initial render does not contain any client-side code.");
	}
}
