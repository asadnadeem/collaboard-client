let socketUrl = "http://localhost:3000";

let socket;

export default {

  connect () {
    socket = io(socketUrl);

    this.startListeningForEvents();
  },

  startListeningForEvents() {
    if (!socket) return;

    socket.on('nameChanged', this.handleNameChangedEvent);
    socket.on('roomChanged', this.handleRoomChangedEvent);

    socket.on('message', this.handleMessageEvent);
    socket.on('all_messages', this.handleAllMessagesEvent);

    socket.on('object', this.handleObjectEvent);
    socket.on('object_update', this.handleObjectUpdateEvent);
    socket.on('delete_object', this.handleDeleteObjectEvent);
    socket.on('object_ack', this.handleObjectAckEvent);
    socket.on('all_objects', this.handleAllObjectsEvent);
  },

  handleNameChangedEvent (name) {
    if (!socket) return;

    let flux = window.flux;

    flux.actions.session.nameChanged(name);
  },

  handleRoomChangedEvent (room) {
    if (!socket) return;

    let flux = window.flux;

    flux.actions.session.roomChanged(room);
  },

  handleMessageEvent (message) {
    if (!socket) return;

    let flux = window.flux;

    flux.actions.chat.receiveMessage(message);
  },

  handleAllMessagesEvent (messages) {
    if (!socket) return;

    let flux = window.flux;

    flux.actions.chat.receiveAllMessages(messages);
  },

  handleObjectEvent (object) {
    if (!socket) return;
    
    let flux = window.flux;

    flux.actions.board.receiveObject(object);
  },


  handleObjectUpdateEvent (object) {
    if (!socket) return;
    
    let flux = window.flux;

    flux.actions.board.objectUpdate(object, true);
  },

  handleDeleteObjectEvent (objectId) {
    if (!socket) return;
    
    let flux = window.flux;

    flux.actions.board.deleteForeignObject(objectId);
  },

  handleObjectAckEvent (object) {
    if (!socket) return;
    
    let flux = window.flux;

    flux.actions.board.objectAck(object);
  },

  handleAllObjectsEvent (objects) {
    if (!socket) return;
    let flux = window.flux;

    flux.actions.board.receiveAllObjects(objects);
  },

  /////

  sendMessage (message) {
    if (!socket) return;

    socket.emit('chat', {
      event: 'message',
      text: message
    });
  },

  sendObject (object) {
    if (!socket) return;

    socket.emit('board', {
      event: 'object',
      object: object
    });
  },

  updateObject (object) {
    if (!socket) return;

    socket.emit('board', {
      event: 'object_update',
      object: object
    })
  },

  deleteObject (objectId) {
    if (!socket) return;

    socket.emit('board', {
      event: 'delete_object',
      objectId: objectId
    });
  },

  changeRoom (room) {
    if (!socket) return;

    socket.emit('joinRoom', room);
  },

  changeName (name) {
    if (!socket) return;

    socket.emit('changeName', name);
  },

  invite (recipient) {
    if (!socket) return;
    
    socket.emit('invite', recipient);
  }

}