import Fluxxor from "fluxxor";
import {CHANGE_NAME, CHANGE_ROOM, NAME_CHANGED, ROOM_CHANGED} from "constants/SessionConstants";

export default Fluxxor.createStore({

  initialize () {
    this._name = null;
    this._room = null;

    this.bindActions(
      CHANGE_NAME, this.handleChangeName,
      CHANGE_ROOM, this.handleChangeRoom,

      NAME_CHANGED, this.handleChangeName,
      ROOM_CHANGED, this.handleChangeRoom,
    );
  },

  handleChangeName(name) {
    this._name = name;

    this.emit("change");
  },

  handleChangeRoom(room) {
    this._room = room;
    this.emit("change");
  },

  getRoom () {
    return this._room;
  },

  getName () {
    return this._name;
  }

});