import BoardStore from "./BoardStore";
import ChatStore from "./ChatStore";
import SessionStore from "./SessionStore";
import ToolboxStore from "./ToolboxStore";

export default {
  BoardStore: new BoardStore(),
  ChatStore: new ChatStore(),
  SessionStore: new SessionStore(),
  ToolboxStore: new ToolboxStore()
};