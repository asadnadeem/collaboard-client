import Fluxxor from "fluxxor";
import {SEND_MESSAGE, RECEIVE_MESSAGE, RECEIVE_ALL_MESSAGES} from "constants/ChatConstants";

export default Fluxxor.createStore({

  initialize () {
    this._messages = [];

    this.bindActions(
      SEND_MESSAGE, this.handleSendMessage,
      RECEIVE_MESSAGE, this.handleReceiveMessage,
      RECEIVE_ALL_MESSAGES, this.handleReceiveAllMessages
    );
  },

  handleSendMessage(message) {
    this._messages.push({
      username: window.flux.stores.SessionStore.getName(),
      text: message,
      own: true
    });

    this.emit("change");
  },

  handleReceiveMessage(message) {
    this._messages.push(message);
    this.emit("change");
  },

  handleReceiveAllMessages(messages) {
    this._messages = messages;
    this.emit("change");
  },

  getMessages () {
    return this._messages;
  }

});