import Fluxxor from "fluxxor";
import {TOOL_SELECT, STROKE_COLOR_CHANGE, FILL_COLOR_CHANGE, STROKE_WIDTH_CHANGE} from "constants/ToolboxConstants";

export default Fluxxor.createStore({

  initialize () {
    this._tools = ["selection", "stroke", "line", "box", "circle", "text", "eraser"];
    this._currFillColor = "#FF0000";
    this._currStrokeColor = "#000000";
    this._currStrokeWidth = 5;
    this._currTool = this._tools[0];

    this.bindActions(
      TOOL_SELECT, this.handleToolSelect,
      FILL_COLOR_CHANGE, this.handleFillColorChange,
      STROKE_COLOR_CHANGE, this.handleStrokeColorChange,
      STROKE_WIDTH_CHANGE, this.handleStrokeWidthChange
    );
  },

  handleToolSelect(tool) {
    this._currTool = tool;

    this.emit("change");
  },

  handleFillColorChange(color) {
    this._currFillColor = color;

    this.emit("change");
  },

  handleStrokeColorChange(color) {
    this._currStrokeColor = color;

    this.emit("change");
  },

  handleStrokeWidthChange(width) {
    this._currStrokeWidth = width;

    this.emit("change");
  },

  getCurrTool () {
    return this._currTool;
  },

  getCurrFillColor () {
    return this._currFillColor;
  },

  getCurrStrokeColor () {
    return this._currStrokeColor;
  },

  getCurrStrokeWidth () {
    return this._currStrokeWidth;
  },

  getTools () {
    return this._tools;
  }

});