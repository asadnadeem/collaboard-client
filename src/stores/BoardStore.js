import Fluxxor from "fluxxor";
import {SEND_OBJECT, RECEIVE_OBJECT, RECEIVE_ALL_OBJECTS,
        UNDO_SELF_OBJECT, OBJECT_ACK, DELETE_OBJECT,
        DELETE_FOREIGN_OBJECT, MOVE_OBJECT, OBJECT_UPDATE} from "constants/BoardConstants";

export default Fluxxor.createStore({

  initialize () {
    this._objects = {};
    this._selfObjects = {};

    this.__counter = 0;

    this.bindActions(
      SEND_OBJECT, this.handleSendObject,
      OBJECT_UPDATE, this.handleObjectUpdate,
      RECEIVE_OBJECT, this.handleReceiveObject,
      DELETE_OBJECT, this.handleDeleteObject,
      DELETE_FOREIGN_OBJECT, this.handleDeleteForeignObject,
      RECEIVE_ALL_OBJECTS, this.handleReceiveAllObjects,
      OBJECT_ACK, this.handleObjectAck
    );
  },

  getNewCreatorLocalId() {
    return this.__counter++;
  },

  handleSendObject(object) {
    object.username = window.flux.stores.SessionStore.getName();
    object.own = true;

    this._selfObjects[object._creatorLocalId] = object;

    this.emit("change");
  },

  handleObjectUpdate(data) {
    let {object, fromServer} = data;

    if (!this._selfObjects[object._id] && !this._objects[object._id]) return;

    if (this._selfObjects[object._id]) {
      this._selfObjects[object._id] = object;
    }

    if (this._objects[object._id] && fromServer) {
      this._objects[object._id] = object;
    }

    this.emit("change");
  },

  handleReceiveObject(object) {
    this._objects[object._id] = object;
    this.emit("change");
  },

  handleObjectAck(object) {
    delete this._selfObjects[object._creatorLocalId];

    this._selfObjects[object._id] = object;
  },

  handleDeleteObject(objectId) {
    delete this._selfObjects[objectId];

    this.emit("change");
  },

  handleDeleteForeignObject(objectId) {
    delete this._objects[objectId];

    this.emit("change");
  },

  handleReceiveAllObjects(objects) {
    this._objects = objects;
    this._selfObjects = {};
    this.emit("change");
  },

  getObjects () {
    let objects = Object.assign({}, this._objects, this._selfObjects);
    let objectKeys = Object.keys(objects);
    objectKeys.sort((a, b) => {
      if (!objects[a].z && objects[a].z !== 0) return 1;
      if (!objects[b].z && objects[b].z !== 0) return -1;

      if (objects[a].z < objects[b].z) return -1;
      if (objects[a].z > objects[b].z) return 1;
      return 0;
    });

    return objectKeys.map((key) => { return objects[key]; });
  },

  getSelfObjects () {
    return this._selfObjects;
  },

  canUndo() {
    return !Object.keys(this._selfObjects).length;
  }

});