export default {
  SEND_OBJECT: 'send_object',
  OBJECT_UPDATE: 'object_update',
  OBJECT_ACK: 'object_ack',
  DELETE_OBJECT: 'delete_object',
  DELETE_FOREIGN_OBJECT: 'delete_foreign_object',
  UNDO_SELF_OBJECT: 'undo_self_object',
  RECEIVE_OBJECT: 'receive_object',
  RECEIVE_ALL_OBJECTS: 'receive_all_objects'
};