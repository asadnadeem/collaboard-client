export default {
  SEND_MESSAGE: 'send_message',
  RECEIVE_MESSAGE: 'receive_message',
  RECEIVE_ALL_MESSAGES: 'receive_all_messages'
};