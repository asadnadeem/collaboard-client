export default {
  CHANGE_NAME: 'change_name',
  CHANGE_ROOM: 'change_room',
  
  NAME_CHANGED: 'name_changed',
  ROOM_CHANGED: 'room_changed',
};