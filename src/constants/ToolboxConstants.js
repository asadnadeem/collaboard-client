export default {
  TOOL_SELECT: 'tool_select',
  FILL_COLOR_CHANGE: 'fill_color_change',
  STROKE_COLOR_CHANGE: 'stroke_color_change',
  STROKE_WIDTH_CHANGE: 'stroke_width_change'
};